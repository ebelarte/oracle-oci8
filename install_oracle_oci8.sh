#!/bin/bash
## Instalar driver OCI8 en Ubuntu
apt-get -y install libaio1
apt-get -y install pear
apt-get -y install php5-dev
dpkg -i oracle-instantclient11.2-basic_11.2.0.4.0-2_amd64.deb
dpkg -i oracle-instantclient11.2-devel_11.2.0.4.0-2_amd64.deb
export ORACLE_HOME=/usr/lib/oracle/11.2/client64
export LD_LIBRARY_PATH=$ORACLE_HOME/lib
export C_INCLUDE_PATH=/usr/lib/oracle/11.2/client
pecl install oci8
echo "[OCI8]" > /etc/php5/mods-available/oci8.conf
echo "extension=oci8.so" >> /etc/php5/mods-available/oci8.conf
php5enmod oci8
service apache2 restart

